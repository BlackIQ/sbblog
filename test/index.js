import SBBLog from "sbblog";

const formatLevels = {
  levels: {
    error: 1,
    warn: 2,
    info: 3,
  },
  colors: {
    error: "red",
    warn: "yellow",
    info: "blue",
  },
};

const logger = new SBBLog(formatLevels.levels);

logger.addConsole(formatLevels.colors);
logger.addFile("./", "hello.log");
// logger.addMongoDB("mongodb://localhost:27017/teststuff", "Package Logs");

logger.log("error", "Heloo", { name: "Amir" });

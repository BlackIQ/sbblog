# SBB Log

**SBB Log** is a simple, light-weight, fast and usefull package to logging logs for your **ReactJs**, **ExpressJs** and **NodeJs** applications.

In this instructure you will learn about how to use this package in your apps.

## Table of content

- [Installation](#installation)
- [Config](#config)
  - [Transports](#transports)
- [Log](#log)
- [Development](#development)

## Installation

How to install **SBB Log**? To install this package, use **NPM**.

```shell
$ npm i sbblog
```

## Config

So, import the package.

```javascript
// ESM 6 and higher
import SBBLog from "sbblog";

// ESM 5 and lower
const SBBLog = require("sbblog");
```

To initializing logger, create your levels of logs. I suggest you to use levels like below:

```javascript
const levels = {
  error: 1,
  warn: 2,
  info: 3,
};
```

now, init logger:

```javascript
const logger = new SBBLog(levels);
```

Ok then, next step is creating transports.

### Transports

We have 3 kind of transports.

- Console
- File
- MongoDB

You can use your own values to initialize any of them. let's start with Console.

#### Console

Console transport just print the output of log in the cli ( console ).

This transport get an array of colors for each level. To create colors just copy data of your levels and replace number with colors. Like below:

```javascript
const colors = {
  error: "red",
  warn: "yellow",
  info: "blue",
};
```

Now, pass it to logger to start console.

```javascript
logger.addConsole(colors);
```

Done! Console transport is now created!

#### File

As you can understand, this is the file transport, just log into the file.

To initialize, pass 2 params. Filename and path. You mas save your file in `/var/logs/application` with file name of `authentication.log`. So, go ahead and create this transport.

```javascript
logger.addFile("/var/logs/application", "authentication.log");
```

From now on, every log save into the database. Let's go one step further and create out transport for MongoDB!

#### MongoDB

Ok, congratulations for passing all steps and comming here. In this trasport, we save data in a database. This time is MongoDB.

Well, knowing just 2 item is enough. The MongoDB connection URL and your collection name that you want to store data.

> In this example, I save data in `AuthLog` collection.

```javascript
logger.addMongoDB("mongodb://localhost:27017/db", "AuthLog");
```

All 3 transports are now created and ready for logging!

## Log

Right now just save your logs with `log()` method. But first let's covers items passing to log.

|  Name   |          Data           |                                  Usage                                  |
| :-----: | :---------------------: | :---------------------------------------------------------------------: |
|  Level  | `info`, `error`, `warn` | Here you describe the type of log. It is one of the levels you created. |
| Message |        `String`         |          Every log has a message, write your log message here.          |
| Context |          `{}`           |        Pass any data you want to save as a context in an object.        |

Now you know items, lets create an info log:

```javascript
logger.log("info", "User logout", { uid: "785457465745648646578778" });
```

Or even an error log:

```javascript
logger.log("error", "Failed to get data", {
  baseUrl: "https://gitlab.com/api/v4/users?username=BlackIQ",
});
```

All done, use it in the right way!

---

## Development

If you want to develop the package, it is so simple. just follow steps below.

- Clone the project
- Install dependencies by running `$ npm install`
- Start changing!
  - Link package
  - Test

> Before you start: **Remember the base or code are stored in `lib/sbblog.js`**. You need to edit there.

### Cloning the project

To clone the project, you need to have git installed. Ok, now clone it same as command below.

```shell
$ git clone https://gitlab.com/BlackIQ/sbblog
```

### installing dependencies

Next, install what package uses with `npm i` or `npm install`.

```shell
$ npm i
```

### Changing

To change package or anything, your need a testing environment to use linked package. Just follow steps.

#### Link package

We asoume you are in `lib` directory. Right. You can open a **tmux** or in another terminal to cd in `test` directory.

In `lib` directory enter link command:

```shell
$ npm link
```

So, in other terminal, or other tmux part, link your development package to your `test` directory. If you are in the `test` directory ok, if not, just say `cd test` and enter the linking command:

```shell
$ npm link sbblog
```

Linking step is done.

#### Test

Your test app is linked. Change anything in package and test it in `test` directory.

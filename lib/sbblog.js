import { createLogger, format, transports, addColors } from "winston";
import { MongoDB } from "winston-mongodb";

class SBBLog {
  constructor(levels) {
    this.logger = createLogger({
      levels: levels,
      exitOnError: false,
    });
  }

  addConsole(colors) {
    addColors(colors);

    this.logger.add(
      new transports.Console({
        format: format.combine(format.colorize(), format.simple()),
      })
    );
  }

  addFile(path, filename) {
    this.logger.add(
      new transports.File({
        filename: `${path}/${filename}`,
        format: format.combine(format.timestamp(), format.prettyPrint()),
      })
    );
  }

  addMongoDB(db, collection) {
    this.logger.add(
      new transports.MongoDB({
        db,
        collection,
        format: format.combine(format.timestamp(), format.metadata()),
        options: {
          useUnifiedTopology: true,
        },
      })
    );
  }

  log(type, message, ctx) {
    this.logger.child({ ctx }).log(type, message);
  }
}

export default SBBLog;
